#ifndef common_h
#define common_h

/* Common data structures for the Racket interpreter project. */

#include <stdbool.h>
#include <stdio.h>

typedef enum {
    booleanType,
    integerType,
    realType,
    stringType,
    consType,
    identifierType,
    functionType,
    uninitType
} VALUE_TYPE;

typedef struct __Value {
    VALUE_TYPE type;
    union {
        bool booleanValue;
        int integerValue;
        double realValue;
        char *stringValue;
        struct __ConsCell *consValue;
        char *identifierValue;
        struct __Function *functionValue;
    };
    struct __Value *next;
    bool quoted, accessible, listed;
} Value;

/* lexing */
int yylex();

void resetTokenizer();

void clearState();

Value *makeValue();

Value *duplicateValue(Value *value);

int descend();

//Make sure to malloc replacement
void descendAndReplace(char *replacement);

int ascend();

void freeValue(Value *value);

void switchStream(FILE *stream);

void printREPLPrompt();

#endif