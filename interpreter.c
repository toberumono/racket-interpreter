#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpreter.h"
#include "frame.h"
#include "common.h"
#include "primitives.h"

#include "helperLibraries/stacks.h"
#include "helperLibraries/stringStuff.h"

Frame *root = NULL;

Value *valueStack = NULL;
FileList *fileList = NULL;
FrameStack *workingFrameStack = NULL;
ValueStack *workingValueStack = NULL; //For values that MUST be kept after a garbage collection

int letrecRunning = 0, mapRunning = 0;

void eval(ConsCell *expr) {
	if (workingFrameStack == NULL)
		workingFrameStack = makeFrameStack();
	if (root == NULL) {
		root = initFrame(NULL);
		bindPrimitives(root);
	}
	ConsCell *current = expr;
	do {
		Value *v = evalInner(current->car, root);
		printConsValueln(v, true, true);
	}
	while (current->cdr->type == consType && (current = current->cdr->consValue)->car->type != uninitType);
	resetInterpreter();
}

Value *evalInner(Value *expr, Frame *env) {
	Value *output = NULL;
	if (expr->quoted) { //If the expression is quoted, we don't evaluate it.
		if (expr->type == consType && expr->consValue->cdr->consValue->car->type != consType && expr->consValue->cdr->consValue->car->type != identifierType && expr->consValue->cdr->consValue->car->type != uninitType)
			output = expr->consValue->cdr->consValue->car;
		else
			output = expr;
	}
	else {
		switch (expr->type) {
			case identifierType:
				if (!strcmp(expr->identifierValue, "if") || !strcmp(expr->identifierValue, "let") || //If this is one of the basic functions,
				        !strcmp(expr->identifierValue, "define") || !strcmp(expr->identifierValue, "lambda") || //it won't have a primitive form,
				        !strcmp(expr->identifierValue, "quote") || !strcmp(expr->identifierValue, "let*") || //so return it as an identifier.
				        !strcmp(expr->identifierValue, "letrec") || !strcmp(expr->identifierValue, "cond") ||
				        !strcmp(expr->identifierValue, "begin") || !strcmp(expr->identifierValue, "display") ||
				        !strcmp(expr->identifierValue, "set!") || !strcmp(expr->identifierValue, "load") ||
				        !strcmp(expr->identifierValue, "map") || !strcmp(expr->identifierValue, "and") ||
				        !strcmp(expr->identifierValue, "or"))
					output = expr;
				else if (!strcmp(expr->identifierValue, "else")) {
					output = makeValue();
					output->type = booleanType;
					output->booleanValue = true;
					pushToValueStack(output);
					return output;
				}
				else {
					output = getVariable(expr->identifierValue, env);
					if (!letrecRunning && output == NULL)
						evalError(concatenate(expr->identifierValue, " is undefined"));
				}
				break;
			case consType:
				if (expr->consValue == NULL)
					evalError("");
				Value *head = evalInnerPop(expr->consValue->car, env);
				if (head->type == functionType) {
					output = evalFunction(head, expr->consValue->cdr, env);
					break;
				}
				if (head->type != identifierType)
					evalError("The first argument in a parenthetic statement must evaluate to an identifier or function");
				char *keyword = head->identifierValue;

				if (!strcmp(keyword, "if"))
					output = evalIf(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "cond"))
					output = evalCond(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "let"))
					output = evalLet(expr->consValue->cdr, env, false);
				else if (!strcmp(keyword, "let*"))
					output = evalLet(expr->consValue->cdr, env, true);
				else if (!strcmp(keyword, "letrec"))
					output = evalLetRec(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "define"))
					output = evalDefine(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "begin"))
					output = evalBegin(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "display"))
					output = evalDisplay(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "set!"))
					output = evalSet(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "load"))
					output = evalLoad(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "map"))
					output = evalMap(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "and"))
					output = evalAnd(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "or"))
					output = evalOr(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "lambda"))
					output = makeClosure(expr->consValue->cdr, env);
				else if (!strcmp(keyword, "quote"))
					output = expr;
				else
					evalError(concatenate(keyword, " is not a valid function name"));
				extern FrameStack *frameStack;
				unmarkFrameStack(frameStack);
				if (!mapRunning)
					unmark();
				markFrameStack(workingFrameStack);
				markWorkingValueStack();
				markValue(output);
				markValue(expr);
				markFrame(env);

				extern ConsCell *tokens;
				Value *temp = makeValue();
				temp->type = consType;
				temp->consValue = tokens;
				markParseTree(temp);
				free(temp);
				sweep();
				break;
			default:
				output = expr;
		}
	}
	pushToValueStack(output);
	return output;
}

Value *evalInnerPop(Value *expr, Frame *env) {
	Value *output = evalInner(expr, env);
	popFromWorkingValueStack();
	return output;
}

Value *evalFunction(Value *head, Value *expr, Frame *env) {
	if (head->type != functionType)
		evalError("");
	pushToWorkingValueStack(expr);
	if (head->functionValue->isPrimitive) {
		Value *args = makeValue();
		Value *arg = args;
		while (expr != NULL && expr->type == consType) {
			arg->type = consType;
			arg->consValue = makeConsCell();
			arg->consValue->car = evalInner(expr->consValue->car, env);
			arg = arg->consValue->cdr;

			expr = expr->consValue->cdr;
		}
		Value *output = head->functionValue->primitive(args);
		popFromWorkingValueStack();
		freePrimitiveMallocs(args);
		return output;
	}

	Frame *functionFrame = generateFunctionEnvironment(head->functionValue, expr, env);
	Value *output = evalInnerPop(head->functionValue->closure, functionFrame);
	pushToValueStack(output);
	popFromFrameStack(workingFrameStack);
	return output;
}

void freePrimitiveMallocs(Value *args) {
	if (args->type == consType) {
		freePrimitiveMallocs(args->consValue->cdr);
		free(args->consValue);
	}
	free(args);
}

Value *evalIf(Value *expr, Frame *env) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 3)
		evalError("An if statement takes 3 arguments of the form: [something that evaluates to a boolean] [what to return on true] [what to return on false]");
	pushToWorkingValueStack(expr);
	bool first = true;
	Value *val = evalInner(expr->consValue->car, env);

	if (val->type == booleanType)
		first = val->booleanValue;
	else if (val->type == identifierType) {
		Value *choice = getVariable(val->identifierValue, env);
		if (choice->type == booleanType)
			first = choice->booleanValue;
		else
			evalError("The first argument in an if statement MUST evaluate to a boolean");
	}
	else
		evalError("The first argument in an if statement MUST evaluate to a boolean");

	if (first)
		return evalInnerPop(expr->consValue->cdr->consValue->car, env);
	else
		return evalInnerPop(expr->consValue->cdr->consValue->cdr->consValue->car, env);
}

Value *evalCond(Value *expr, Frame *env) {
	basicChecks(expr);
	pushToWorkingValueStack(expr);
	while (expr->consValue->car->type == consType) {
		if (expr->consValue->car->consValue->cdr->type != consType)
			evalError("A cond case must be of the form: [something that evaluates to a boolean] [expression]");
		Value *run = evalInner(expr->consValue->car->consValue->car, env);
		if (run->type != booleanType)
			evalError("The first item in a cond case must evaluate to a boolean");
		if (run->booleanValue)
			return evalInnerPop(expr->consValue->car->consValue->cdr->consValue->car, env);
		if (expr->consValue->cdr->type == uninitType)
			break;
		if (expr->consValue->cdr->type != consType) //In case a pair or improper list was passed
			evalError("");
		expr = expr->consValue->cdr;
	}
	popFromWorkingValueStack();
	Value *output = makeValue();
	output->type = uninitType;
	pushToValueStack(output);
	return output;
}

Value *evalLet(Value *expr, Frame *env, bool star) {
	basicChecks(expr);
	if (expr->consValue->car->type != consType)
		evalError("The first argument in a let statement must be a list of bindings enclosed within parentheses");
	if (lengthOfConsCell(expr->consValue) != 2 || expr->consValue->car->type != consType || expr->consValue->car->consValue->car->type != consType)
		evalError("A let expression takes 2 arguments of the form: [list of bindings] [expression]");
	Frame *previous = env;
	env = initFrame(env);
	pushToWorkingValueStack(expr);

	ConsCell *bindings = expr->consValue->car->consValue;
	while (bindings->car->type == consType) {
		if (bindings->car->consValue->cdr->type == uninitType || bindings->car->consValue->car->type != identifierType)
			evalError("Invalid variable assignment");
		if (!star && containsVariable(bindings->car->consValue->car->identifierValue, env))
			evalError(concatenate(bindings->car->consValue->car->identifierValue, " is already assigned in this let expression"));
		Value *val = evalInner(bindings->car->consValue->cdr->consValue->car, star ? env : previous);
		addVariable(bindings->car->consValue->car->identifierValue, val, env);

		if (bindings->cdr->type == uninitType)
			break;
		if (bindings->cdr->type != consType) //In case a pair or improper list was passed
			evalError("");
		bindings = bindings->cdr->consValue;
	}

	if (expr->consValue->cdr->type != consType)
		evalError("");
	Value *output = evalInnerPop(expr->consValue->cdr->consValue->car, env);
	extern FrameStack *frameStack;
	if (!(env->function))
		freeFrame(popFromFrameStack(frameStack));
	env = previous;
	return output;
}

Value *evalLetRec(Value *expr, Frame *env) {
	basicChecks(expr);
	if (expr->consValue->car->type != consType)
		evalError("The first argument in a let statement must be a list of bindings enclosed within parentheses");
	if (lengthOfConsCell(expr->consValue) != 2 || expr->consValue->car->type != consType || expr->consValue->car->consValue->car->type != consType)
		evalError("A let expression takes 2 arguments of the form: [list of bindings] [expression]");
	Frame *previous = env;
	env = initFrame(env);
	pushToWorkingValueStack(expr);

	ConsCell *bindings = expr->consValue->car->consValue;
	Variable *variables[lengthOfConsCell(bindings)];
	int i = 0;
	while (bindings->car->type == consType) {
		if (bindings->car->consValue->cdr->type == uninitType || bindings->car->consValue->car->type != identifierType)
			evalError("Invalid variable assignment");
		if (containsVariable(bindings->car->consValue->car->identifierValue, env))
			evalError(concatenate(bindings->car->consValue->car->identifierValue, " is already assigned in this let expression"));

		Value *tempValue = makeValue();
		pushToValueStack(tempValue);
		addVariable(bindings->car->consValue->car->identifierValue, tempValue, env);
		variables[i++] = env->vars;
		if (bindings->cdr->type == uninitType)
			break;
		if (bindings->cdr->type != consType) //In case a pair or improper list was passed
			evalError("");
		bindings = bindings->cdr->consValue;
	}

	letrecRunning = true;
	i = 0;
	bindings = expr->consValue->car->consValue;
	while (bindings->car->type == consType) {
		if (bindings->car->consValue->cdr->type == uninitType)
			evalError("Invalid variable assignment");

		variables[i]->value = evalInner(bindings->car->consValue->cdr->consValue->car, env);
		if (bindings->cdr->type == uninitType)
			break;
		if (bindings->cdr->type != consType) //In case a pair or improper list was passed
			evalError("");

		bindings = bindings->cdr->consValue;
		i++;
	}
	letrecRunning = false;

	if (expr->consValue->cdr->type != consType)
		evalError("");
	Value *output = evalInnerPop(expr->consValue->cdr->consValue->car, env);
	extern FrameStack *frameStack;
	if (!(env->function))
		freeFrame(popFromFrameStack(frameStack));
	env = previous;
	return output;
}

Value *evalBegin(Value *expr, Frame *env) {
	basicChecks(expr);
	pushToWorkingValueStack(expr);
	if (lengthOfConsCell(expr->consValue) == 0) {
		Value *val = makeValue();
		pushToValueStack(val);
		return val;
	}
	Value *output;
	while (expr->consValue->car->type == consType) {
		output = evalInner(expr->consValue->car, env);

		if (expr->consValue->cdr->type == uninitType)
			break;
		if (expr->consValue->cdr->type != consType)
			evalError("");
		expr = expr->consValue->cdr;
	}
	popFromWorkingValueStack();
	return output;
}

Value *evalDisplay(Value *expr, Frame *env) {
	pushToWorkingValueStack(expr);
	while (expr->type == consType) {
		printConsValue(evalInner(expr->consValue->car, env), true, false);
		expr = expr->consValue->cdr;
	}
	popFromWorkingValueStack();
	Value *output = makeValue();
	output->type = uninitType;
	pushToValueStack(output);
	return output;
}

Value *evalDefine(Value *expr, Frame *env) {
	basicChecks(expr);
	pushToWorkingValueStack(expr);
	if (lengthOfConsCell(expr->consValue) != 2 || expr->consValue->car->type != identifierType)
		evalError("A define expression takes 2 arguments of the form: [binding] [expression]");
	addVariable(expr->consValue->car->identifierValue, evalInnerPop(expr->consValue->cdr->consValue->car, env), getTopFrame(env));
	Value *output = makeValue();
	output->type = uninitType;
	pushToValueStack(output);
	return output;
}

Value *evalSet(Value *expr, Frame *env) {
	basicChecks(expr);
	pushToWorkingValueStack(expr);
	if (lengthOfConsCell(expr->consValue) != 2 || expr->consValue->car->type != identifierType)
		evalError("A define expression takes 2 arguments of the form: [binding] [expression]");
	if (!setVariable(expr->consValue->car->identifierValue, evalInnerPop(expr->consValue->cdr->consValue->car, env), env))
		evalError("The variable specified in set! must have already been bound to a value");
	Value *output = makeValue();
	output->type = uninitType;
	pushToValueStack(output);
	return output;
}

Value *evalLoad(Value *expr, Frame *env) {
	basicChecks(expr);
	env = getTopFrame(env);
	while (expr->type == consType) {
		if (expr->consValue->car->type != stringType)
			evalError("All file paths must be strings");
		loadFile(expr->consValue->car->stringValue);
		expr = expr->consValue->cdr;
	}
	Value *output = makeValue();
	output->type = uninitType;
	pushToValueStack(output);
	return output;
}

Value *mapRecursion(Value *function, Value *lists, Frame *env) {
	Value *args = makeValue();
	Value *head = lists;
	Value *arg = args;
	while (head->type == consType) {
		arg->type = consType;
		arg->consValue = makeConsCell();
		arg->consValue->car = unquote(head->consValue->car->consValue->car);
		head->consValue->car = head->consValue->car->consValue->cdr;
		head = head->consValue->cdr;
		arg = arg->consValue->cdr;
	}

	Value *first = evalFunction(function, args, env);
	pushToWorkingValueStack(first);
	freePrimitiveMallocs(args);
	Value *next = (lists->consValue->car->type == consType) ? mapRecursion(function, lists, env) : NULL;
	Value *output = makeValue();
	output->type = consType;
	output->consValue = makeConsCell();
	output->consValue->car = first;
	if (next != NULL) {
		freeValue(output->consValue->cdr);
		output->consValue->cdr = next;
	}
	popFromWorkingValueStack();
	pushToValueStack(output);
	return output;
}

Value *evalMap(Value *expr, Frame *env) {
	mapRunning++;
	basicChecks(expr);
	pushToWorkingValueStack(expr);

	Value *function = evalInner(expr->consValue->car, env);
	expr = expr->consValue->cdr;
	Value *head = expr;
	Value *lists = makeValue();
	Value *list = lists;

	while (head->type == consType) {
		list->type = consType;
		list->consValue = makeConsCell();
		list->consValue->car = unquote(evalInner(head->consValue->car, env));
		list = list->consValue->cdr;
		head = head->consValue->cdr;
	}

	Value *output = mapRecursion(function, lists, env);
	output->quoted = true;
	pushToValueStack(output);
	popFromWorkingValueStack();
	freePrimitiveMallocs(lists);
	mapRunning--;
	return output;
}

Value *evalAnd(Value *expr, Frame *env) {
	basicChecks(expr);
	pushToWorkingValueStack(expr);
	while (expr->type == consType) {
		Value *item = evalInner(expr->consValue->car, env);
		if (item->type != booleanType)
			continue;

		if (!item->booleanValue) {
			Value *output = makeValue();
			output->type = booleanType;
			output->booleanValue = false;
			pushToValueStack(output);
			return output;
		}
		if (expr->consValue->cdr->type == uninitType)
			break;
		if (expr->consValue->cdr->type != consType)
			evalError("");
		expr = expr->consValue->cdr;
	}

	Value *output = makeValue();
	output->type = booleanType;
	output->booleanValue = true;
	pushToValueStack(output);
	popFromWorkingValueStack();
	return output;
}

Value *evalOr(Value *expr, Frame *env) {
	basicChecks(expr);
	pushToWorkingValueStack(expr);
	while (expr->type == consType) {
		Value *item = evalInner(expr->consValue->car, env);

		if (item->type != booleanType || item->booleanValue) {
			Value *output = makeValue();
			output->type = booleanType;
			output->booleanValue = true;
			pushToValueStack(output);
			return output;
		}
		if (expr->consValue->cdr->type == uninitType)
			break;
		if (expr->consValue->cdr->type != consType)
			evalError("");
		expr = expr->consValue->cdr;
	}

	Value *output = makeValue();
	output->type = booleanType;
	output->booleanValue = false;
	pushToValueStack(output);
	popFromWorkingValueStack();
	return output;
}

void evalError(char *message) {
	printf("Syntax Error");
	if (message[0])
		printf(": %s", message);
	printf("\n");
	freeFileList();
	resetInterpreter();
	resetTokenizer();
	root = NULL;
	exit(EXIT_FAILURE);
}

void resetInterpreter() {
	freeFrameStack(workingFrameStack);
	workingFrameStack = NULL;
	freeWorkingValueStack();
	letrecRunning = 0;
	mapRunning = 0;
}

void basicChecks(Value *expr) {
	if (expr->type != consType)
		evalError("");
}

void pushToValueStack(Value *value) {
	if (value == NULL || value->listed)
		return;
	value->listed = true;
	value->next = valueStack;
	valueStack = value;
	if (value->type == consType) {
		pushToValueStack(value->consValue->car);
		pushToValueStack(value->consValue->cdr);
	}
}

Value *popFromValueStack() {
	if (valueStack == NULL)
		return NULL;
	Value *temp = valueStack;
	valueStack = valueStack->next;
	temp->next = NULL;
	temp->listed = false;
	return temp;
}

void freeValueStack() {
	while (valueStack != NULL)
		freeValue(popFromValueStack());
}

bool isQuoted(Value *expr) {
	return expr->type == consType && lengthOfConsCell(expr->consValue) == 2 &&
	       expr->consValue->car->type == identifierType && !strcmp(expr->consValue->car->identifierValue, "quote");
}

Value *unquote(Value *expr) {
	if (isQuoted(expr))
		return expr->consValue->cdr->consValue->car;
	return expr;
}

bool compareValues(Value *expr, Value *other) {
	if (expr->type != other->type)// || expr->quoted != other->quoted)
		return false;
	switch (expr->type) {
		case integerType:
			return expr->integerValue == other->integerValue;
		case realType:
			return expr->realValue == other->realValue;
		case stringType:
			return !strcmp(expr->stringValue, other->stringValue);
		case booleanType:
			return expr->booleanValue == other->booleanValue;
		case consType:
			return compareValues(expr->consValue->car, other->consValue->car) && compareValues(expr->consValue->cdr, other->consValue->cdr);
		case identifierType:
			return !strcmp(expr->identifierValue, other->identifierValue);
		case functionType:
			return compareFunctions(expr->functionValue, other->functionValue);
		default:
			return true;
	}
}

void freeFileList() {
	while (fileList != NULL) {
		FileList *temp = fileList;
		fileList = fileList->next;
		free(temp);
	}
}

void loadFile(char *path) {
	if (isOpenedFile(path))
		return;
	FileList *newFile = (FileList *) malloc(sizeof(FileList));
	newFile->next = fileList;
	newFile->path = path;
	fileList = newFile;
	FILE *stream = fopen(path, "r");
	if (stream == NULL) {
		char *tempString = concatenate("The file at ", path);
		char *errorString = concatenate(tempString, " cannot be opened or does not exist");
		free(tempString);
		evalError(errorString);
	}
	switchStream(stream);
	while (yylex());
	fclose(stream);
	switchStream(stdin);
	extern ConsCell *head;
	while (head->cdr->type == consType)
		head = head->cdr->consValue;
}

bool isOpenedFile(char *path) {
	FileList *current = fileList;
	while (current != NULL) {
		if (!strcmp(current->path, path))
			return true;
		current = current->next;
	}
	return false;
}

void markParseTree(Value *treeNode) {
	if (treeNode->type == consType) {
		markParseTree(treeNode->consValue->car);
		markParseTree(treeNode->consValue->cdr);
	}
	treeNode->accessible = true;
}

void unmark() {
	Value *head = valueStack;
	while (head != NULL) {
		unmarkValue(head);
		head = head->next;
	}

}

void unmarkValue(Value *value) {
	if (value == NULL)
		return;
	value->accessible = false;
	if (value->type == consType) {
		unmarkValue(value->consValue->car);
		unmarkValue(value->consValue->cdr);
	}
	else if (value->type == functionType)
		unmarkFunction(value->functionValue);
}

void unmarkFunction(Function *function) {
	if (function == NULL || function->isPrimitive)
		return;
	unmarkFrame(function->env);
	unmarkValue(function->closure);
}

void unmarkFrame(Frame *frame) {
	if (frame == NULL || !frame->accessible)
		return;
	frame->accessible = false;
	Variable *var = frame->vars;
	while (var != NULL) {
		unmarkValue(var->value);
		var = var->next;
	}
	if (frame->previous != NULL)
		unmarkFrame(frame->previous);
}

void markValue(Value *value) {
	if (value == NULL)
		return;
	value->accessible = true;
	if (value->type == consType) {
		markValue(value->consValue->car);
		markValue(value->consValue->cdr);
	}
	else if (value->type == functionType)
		markFunction(value->functionValue);
}

void markFunction(Function *function) {
	if (function == NULL || function->isPrimitive)
		return;
	markFrame(function->env);
	markValue(function->closure);
}

void markFrame(Frame *frame) {
	if (frame == NULL || frame->accessible)
		return;
	frame->accessible = true;
	Variable *var = frame->vars;
	while (var != NULL) {
		markValue(var->value);
		var = var->next;
	}
	if (frame->previous != NULL)
		markFrame(frame->previous);
}

void freeValueSweep(Value *value) {
	switch (value->type) { // frees the malloc'ed cdr if its type is string, cons, or identifierType
		case stringType:
			free(value->stringValue);
			break;
		case consType:
			free(value->consValue);
			break;
		case identifierType:
			free(value->identifierValue);
			break;
		case functionType:
			freeFunction(value->functionValue);
			break;
		default:
			break;
	}
	free(value);
	value = NULL;
}

void sweep() {
	Value *head = valueStack;
	Value *previous = NULL;
	while (head != NULL) {
		if (!head->accessible) {
			Value *next = head->next;
			freeValueSweep(head);
			if (previous == NULL)
				valueStack = next;
			else
				previous->next = next;
			head = next;
		}
		else {
			previous = head;
			head = head->next;
		}
	}
	sweepFrames();
}

void freeWorkingValueStack() {
	while (workingValueStack != NULL)
		popFromWorkingValueStack();
}

void pushToWorkingValueStack(Value *value) {
	ValueStack *newItem = (ValueStack *) malloc(sizeof(ValueStack));
	newItem->value = value;
	newItem->next = workingValueStack;
	workingValueStack = newItem;
}

void popFromWorkingValueStack() {
	if (workingValueStack == NULL)
		return;
	ValueStack *temp = workingValueStack->next;
	free(workingValueStack);
	workingValueStack = temp;
}

void markWorkingValueStack() {
	ValueStack *head = workingValueStack;
	while (head != NULL) {
		markValue(head->value);
		head = head->next;
	}
}