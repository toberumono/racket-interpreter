(define begin-test
  (lambda (x)
    (begin
      (let* ((y #t) (z (not y)))
        (if z y x)))))

(begin-test 3)
(begin-test "hi")
(begin-test #f)
