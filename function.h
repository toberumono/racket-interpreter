#ifndef function_h
#define function_h

#include "frame.h"
#include "common.h"

typedef struct __Parameter {
	char *name;
	struct __Parameter *next;
} Parameter;

typedef struct __Function {
	int numParams;

	struct __Parameter *parameters;
	struct __Frame *env;
	union {
		Value *closure;
		Value *(*primitive)(Value *args);
	};
	bool isPrimitive;
} Function;

Value *makeClosure(Value *lambda, struct __Frame *env);

Parameter *makeParameter(char *name);

Value *makePrimitive(Value *(*primitive)(Value *));

struct __Frame *generateFunctionEnvironment(Function *function, Value *params, struct __Frame *caller);

//Initializes an empty non-primitive function
Function *makeFunction(struct __Frame *env);

//Frees the function, but NOT the variables in the base frame
void freeFunction(Function *function);

//Frees the parameters list in a Function
void freeParameterList(Parameter *parameter);

Function *duplicateFunction(Function *function);

void quoteAll(Value *expr);

bool compareFunctions(Function *first, Function *second);

#endif