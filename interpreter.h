#ifndef interpreter_h
#define interpreter_h

#include "common.h"
#include "consCell.h"
#include "frame.h"

typedef struct __FileList {
	char *path;
	struct __FileList *next;
} FileList;

typedef struct __ValueStack {
	Value *value;

	struct __ValueStack *next;
} ValueStack;

void eval(ConsCell *expr);

Value *evalInner(Value *expr, Frame *env);

Value *evalInnerPop(Value *expr, Frame *env);

Value *evalFunction(Value *head, Value *expr, Frame *env);

Value *evalIf(Value *expr, Frame *env);

Value *evalCond(Value *expr, Frame *env);

Value *evalLet(Value *expr, Frame *env, bool star);

Value *evalLetRec(Value *expr, Frame *env);

Value *evalBegin(Value *expr, Frame *env);

Value *evalDisplay(Value *expr, Frame *env);

Value *evalDefine(Value *expr, Frame *env);

Value *evalSet(Value *expr, Frame *env);

Value *evalLoad(Value *expr, Frame *env);

Value *evalMap(Value *expr, Frame *env);

Value *evalAnd(Value *expr, Frame *env);

Value *evalOr(Value *expr, Frame *env);

void evalError(char *message);

void resetInterpreter();

//Run this at the start of all functions
void basicChecks(Value *expr);

void freePrimitiveMallocs(Value *args);

void pushToValueStack(Value *value);

Value *popFromValueStack();

void freeValueStack();

bool isQuoted(Value *expr);

Value *unquote(Value *expr);

bool compareValues(Value *expr, Value *other);

void freeFileList();

void loadFile(char *path);

bool isOpenedFile(char *path);

void markParseTree(Value *treeNode);

void unmark();

void unmarkValue(Value *value);

void unmarkFunction(Function *function);

void unmarkFrame(Frame *frame);

void markValue(Value *value);

void markFunction(Function *function);

void markFrame(Frame *frame);

void sweep();

void freeWorkingValueStack();

void pushToWorkingValueStack(Value *value);

void popFromWorkingValueStack();

void markWorkingValueStack();

#endif