#include <string.h>

#include "interpreter.h"
#include "common.h"
#include "frame.h"
#include "helperLibraries/stringStuff.h"

Value *makeClosure(Value *lambda, Frame *env) {
	basicChecks(lambda);
	if (lambda->consValue->car->type != consType)
		evalError("lambda expects a list of variables and an expression");
	if (lengthOfConsCell(lambda->consValue) != 2)
		evalError("lambda expects a list of variables and an expression");
	Function *function = makeFunction(env);

	ConsCell *varPointer = lambda->consValue->car->consValue;
	Parameter *head = NULL;
	while (varPointer->car->type == identifierType) {
		function->numParams++;
		if(function->parameters == NULL) {
			head = makeParameter(varPointer->car->identifierValue);
			function->parameters = head;
		}
		else {
			head->next = makeParameter(varPointer->car->identifierValue);
			head = head->next;
		}
		if (varPointer->cdr->type != consType)
			break;
		varPointer = varPointer->cdr->consValue;
	}
	function->closure = lambda->consValue->cdr->consValue->car;
	Value *func = makeValue();
	func->type = functionType;
	func->functionValue = function;
	pushToValueStack(func);
	return func;
}

Parameter *makeParameter(char *name) {
	Parameter *parameter = (Parameter *) malloc (sizeof(Parameter));
	parameter->name = name;
	parameter->next = NULL;
	return parameter;
}

Value *makePrimitive(Value *(*primitive)(Value *)) {
	Function *function = (Function *) malloc(sizeof(Function));
	function->parameters = NULL;
	function->isPrimitive = true;
	function->primitive = primitive;
	Value *result = makeValue();
	result->type = functionType;
	result->functionValue = function;
	return result;
}

Frame *generateFunctionEnvironment(Function *function, Value *params, Frame *caller) {
	Frame *env = initFrame(function->env); //Clone the base so that this function won't be changed while running.

	pushToWorkingValueStack(params);
	extern FrameStack *workingFrameStack;
	pushToFrameStack(env, workingFrameStack);
	if (function->numParams != 0) {
		if (params == NULL || params->type != consType)
			evalError("There was an invalid number of parameters in the function call");
		if (lengthOfConsCell(params->consValue) != function->numParams) //If the number of params don't match, throw an error
			evalError("There was an invalid number of parameters in the function call");
		Parameter *param = function->parameters;
		while (params->consValue->car->type != uninitType) { //Copy in the data
			addVariable(param->name, evalInner(params->consValue->car, caller), env);

			if (params->consValue->cdr->type != consType)
				break;
			params = params->consValue->cdr;
			param = param->next;
		}
	}
	return env;
}

Function *makeFunction(Frame *env) {
	Function *function = (Function *) malloc(sizeof(Function));
	function->parameters = NULL;
	markFunctionEnv(env);
	function->env = env;
	function->numParams = 0;
	function->isPrimitive = false;
	return function;
}

void freeFunction(Function *function) {
	if (function->parameters != NULL)
		freeParameterList(function->parameters);
	free(function);
	function = NULL;
}

void freeParameterList(Parameter *parameter) {
	if (parameter->next != NULL)
		freeParameterList(parameter->next);
	free(parameter);
}

Parameter *duplicateParameter(Parameter *parameter) {
	Parameter *result = (Parameter *) malloc(sizeof(Parameter));
	result->name = duplicateString(parameter->name);
	if (parameter->next == NULL) {
		result->next = NULL;
		return result;
	}
	result->next = duplicateParameter(parameter->next);
	return result;
}

Function *duplicateFunction(Function *function) {
	Function *result = (Function *) malloc(sizeof(Function));
	result->numParams = function->numParams;
	result->isPrimitive = function->isPrimitive;
	result->env = function->env;
	result->parameters = duplicateParameter(function->parameters);
	if (function->isPrimitive)
		result->primitive = function->primitive;
	else
		result->closure = function->closure;
	return result;
}

void quoteAll(Value *expr) {
	if (expr == NULL)
		evalError("");
	if (expr->type == consType){
		quoteAll(expr->consValue->car);
		quoteAll(expr->consValue->cdr);
	}
	expr->quoted = true;
}

bool compareFunctions(Function *first, Function *second) {
	if (first->isPrimitive != second->isPrimitive || first->numParams != second->numParams)
		return false;
	Parameter *param1 = first->parameters;
	Parameter *param2 = second->parameters;
	while (param1 != NULL && param2 != NULL) {
		if (!strcmp(param1->name, param2->name))
			return false;
		param1 = param1->next;
		param2 = param2->next;
	}
	if (param1 == NULL ^ param2 == NULL)
		return false;
	if (!compareFrames(first->env, second->env))
		return false;
	return true;
}