
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpreter.h"
#include "frame.h"
#include "common.h"
#include "helperLibraries/stacks.h"
#include "helperLibraries/stringStuff.h"

FrameStack *frameStack = NULL;

Frame *initFrame(Frame *env) {
	Frame *frame = (Frame *) malloc(sizeof(Frame));
	frame->previous = env;
	frame->vars = NULL;
	frame->function = false;
	frame->accessible = true;
	if (frameStack == NULL)
		frameStack = makeFrameStack();
	pushToFrameStack(frame, frameStack);
	return frame;
}

Variable *initVariable(char *name, Value *value) {
	Variable *var = (Variable *) malloc(sizeof(Variable));
	var->next = NULL;
	var->name = name;
	var->value = value;
	return var;
}

void addVariable(char *name, Value *value, Frame *env) {
	Variable *var = initVariable(name, value);
	var->next = env->vars;
	env->vars = var;
}

bool setVariable(char *name, Value *value, Frame *env) {
	while (env != NULL) {
		Variable *var = env->vars;
		while (var != NULL) {
			if (!strcmp(name, var->name)) {
				var->value = value;
				return true;
			}
			var = var->next;
		}
		env = env->previous;
	}
	return false;
}

Value *getVariable(char *name, Frame *env) {
	Variable *var = env->vars;
	while (var != NULL && strcmp(var->name, name))
		var = var->next;

	if (var != NULL && !strcmp(var->name, name))
		return var->value;
	else if (env->previous != NULL)
		return getVariable(name, env->previous);
	else
		evalError(concatenate(name, " is an unbound identifier."));
	return makeValue();
}

bool containsVariable(char *name, Frame *env) {
	Variable *var = env->vars;
	while (var != NULL && strcmp(var->name, name))
		var = var->next;

	if (var != NULL && !strcmp(var->name, name))
		return true;
	return false;
}

bool isVariable(char *name, Frame *env) {
	while (env != NULL) {
		if (containsVariable(name, env))
			return true;
		env = env->previous;
	}
	return false;
}

void freeVariable(Variable *var) {
	if (var->next != NULL)
		freeVariable(var->next);
	free(var);
}

void freeFrame(Frame *env) {
	if (env->vars != NULL)
		freeVariable(env->vars);
	free(env);
}

Frame *getTopFrame(Frame *env) {
	if (env->previous == NULL)
		return env;
	return env->previous;
}

void markFunctionEnv(Frame *env) {
	if (env == NULL || env->function)
		return;
	env->function = true;
	if (env->previous != NULL)
		markFunctionEnv(env->previous);
}

bool compareFrames(Frame *first, Frame *second) {
	Variable *var1 = first->vars;
	Variable *var2 = second->vars;
	while (var1 != NULL && var2 != NULL) {
		if (!strcmp(var1->name, var2->name))
			return false;
		if (!compareValues(var1->value, var2->value))
			return false;
		var1 = var1->next;
		var2 = var2->next;
	}
	if (var1 == NULL ^ var2 == NULL)
		return false;
	return true;
}

void sweepFrames() {
	for (int i = 0; i < frameStack->size; i++) {
		Variable *var = frameStack->stack[i]->vars;
		Variable *previous = NULL;
		while (var != NULL) {
			if (var->value == NULL) {
				Variable *next = var->next;
				free(var);
				if (previous == NULL)
					frameStack->stack[i]->vars = next;
				else
					previous->next = next;
				var = next;
			}
			else {
				previous = var;
				var = var->next;
			}
		}
	}
}

FrameStack *makeFrameStack() {
	FrameStack *output = (FrameStack *) malloc(sizeof(FrameStack));
	output->maxSize = 10;
	output->stack = (Frame **) malloc(output->maxSize * sizeof(Frame *));
	output->size = 0;
	return output;
}

void pushToFrameStack(Frame *env, FrameStack *stack) {
	if (stack->size + 1 >= stack->maxSize) {
		stack->stack = (Frame **) realloc(stack->stack, stack->maxSize * 2 * sizeof(Frame *));
		stack->maxSize = stack->maxSize * 2;
	}
	stack->stack[stack->size] = env;
	stack->size = stack->size + 1;
}

Frame *popFromFrameStack(FrameStack *stack) {
	if (stack->size > 0)
		stack->size--;
	return stack->stack[stack->size];
}

void unmarkFrameStack(FrameStack *stack) {
	for (int i = 0; i < stack->size; i++)
		unmarkFrame(stack->stack[i]);
}

void markFrameStack(FrameStack *stack) {
	for (int i = 0; i < stack->size; i++)
		markFrame(stack->stack[i]);
}

void freeFrameStack(FrameStack *stack) {
	if (stack == NULL)
		return;
	free(stack->stack);
	free(stack);
	stack = NULL;
}