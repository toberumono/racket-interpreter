#include <stdlib.h>
#include <string.h>

#include "consCell.h"
#include "common.h"
#include "function.h"
#include "interpreter.h"
#include "helperLibraries/escape.h"

int numQuotes = 0;

void freeConsList(ConsCell *node) { // since space is only malloc'ed for string, cons, and identifier Types,
	// checks to see if the ConsCell type is one of those, and frees if necessary
	if (node == NULL)
		return;
	freeValue(node->car);

	freeValue(node->cdr);

	free(node);
}

void printConsCell(ConsCell *node) {
	if (node == NULL)
		return;
	printValueln(node->car);
	printValueln(node->cdr);
}

void printValue(Value *value) { // check the type of the value,
	// then print the token and its corresponding type
	switch (value->type) {
		case booleanType:
			printf((value->booleanValue ? "#t" : "#f"));
			break;
		case integerType:
			printf("%i", value->integerValue);
			break;
		case realType:
			printf("%f", value->realValue);
			break;
		case stringType:
			printf("%s", value->stringValue);
			break;
		case consType:
			printConsCell(value->consValue);
			break;
		case identifierType:
			printf("%s", value->identifierValue);
			break;
		case functionType:
			printf("function");
			break;
		default:
			break;
	}
}

void printValueln(Value *value) {
	printValue(value);
	if (value->type != uninitType)
		printf("\n");
	numQuotes = 0;
}

void printTokenizedInput(ConsCell *node, bool unescape, bool quoteString) {
	if (node->car == NULL || node->cdr == NULL)
		return;
	printConsValue(node->car, unescape, quoteString);

	if (node->car->type != uninitType && node->cdr->type != uninitType)
		printf(" ");

	if (node->cdr->type == consType)
		printTokenizedInput(node->cdr->consValue, unescape, quoteString);
	else if (node->cdr->type != uninitType) {
		printf(". ");
		printConsValue(node->cdr, unescape, quoteString);
	}
}

void printTokenizedInputln(ConsCell *node, bool unescape, bool quoteString) {
	printTokenizedInput(node, unescape, quoteString);
	printf("\n");
	numQuotes = 0;
}

void printConsValueln(Value *value, bool unescape, bool quoteString) {
	if (value == NULL)
		evalError("");
	if (value->type == uninitType)
		return;
	printConsValue(value, unescape, quoteString);
	printf("\n");
	numQuotes = 0;
}

void printConsValue(Value *value, bool unescapeString, bool quoteString) {
	bool reduceQuote = false;
	switch (value->type) {
		case booleanType:
			printf(value->booleanValue ? "#t" : "#f");
			break;
		case integerType:
			printf("%i", value->integerValue);
			break;
		case realType:
			printf("%f", value->realValue);
			break;
		case stringType:
			if (unescapeString)
				printf(quoteString ? "\"%s\"" : "%s", value->stringValue);
			else {
				char *string = escape(value->stringValue);
				printf("\"%s\"", string);
				free(string);
			}
			break;
		case functionType:
			printf("function");
			break;
		case consType:
			numQuotes++;
			reduceQuote = true;
			int quotes = 0;
			while (isQuoted(value)) {
				value = unquote(value);
				quotes++;
			}
			if (value->type == consType || value->type == identifierType)
				for (int i = 0; i < quotes; i++)
					printf("'");
			else
				for (int i = 0; i < quotes - 1; i++)
					printf("'");
			if (quotes == 0 && value->quoted)
				printf("'");
			if (value->type == consType) {
				printf("(");
				printTokenizedInput(value->consValue, unescapeString, quoteString);
				printf(")");
			}
			else
				printConsValue(value, unescapeString, quoteString);
			break;
		case identifierType:
			if (numQuotes == 0 && value->quoted)
				printf("'");
			printf("%s", value->identifierValue);
			break;
		default:
			break;
	}
	if (reduceQuote && numQuotes > 0)
		numQuotes--;
}

int lengthOfConsCell(ConsCell *node) {
	if (node->cdr->type == consType)
		return 1 + lengthOfConsCell(node->cdr->consValue);
	return 1;
}

ConsCell *makeConsCell() {
	ConsCell *output = (ConsCell *) malloc(sizeof(ConsCell));
	output->cdr = makeValue();
	return output;
}