CC = clang
CFLAGS = -g

default: tokenizer

tokenizer: tokenizer.o tokenizer_main.o escape.o stacks.o consCell.o stringStuff.o frame.o interpreter.o function.o primitives.o
	$(CC) $(CFLAGS) -o tokenizer tokenizer.o tokenizer_main.o escape.o stacks.o consCell.o stringStuff.o frame.o interpreter.o function.o primitives.o

escape.o: ./helperLibraries/escape.c ./helperLibraries/escape.h
	$(CC) -c $(CFLAGS) ./helperLibraries/escape.c

stacks.o: ./helperLibraries/stacks.c ./helperLibraries/stacks.h common.h
	$(CC) -c $(CFLAGS) ./helperLibraries/stacks.c

stringStuff.o: ./helperLibraries/stringStuff.c ./helperLibraries/stringStuff.h
	$(CC) -c $(CFLAGS) ./helperLibraries/stringStuff.c

tokenizer.o: tokenizer.c tokenizer.l
	$(CC) -c $(CFLAGS) tokenizer.c

consCell.o: consCell.c consCell.h common.h
	$(CC) -c $(CFLAGS) consCell.c

frame.o: frame.c interpreter.h consCell.h common.h
	$(CC) -c $(CFLAGS) frame.c

function.o: frame.h consCell.h common.h function.c
	$(CC) -c $(CFLAGS) function.c

primitives.o: common.h primitives.c
	$(CC) -c $(CFLAGS) primitives.c

interpreter.o: interpreter.c frame.h consCell.h common.h
	$(CC) -c $(CFLAGS) interpreter.c

tokenizer_main.o: tokenizer_main.c common.h
	$(CC) -c $(CFLAGS) tokenizer_main.c

clean:
	$(RM) tokenizer ./helperLibraries/*.o *.o *~ tokenizer.c