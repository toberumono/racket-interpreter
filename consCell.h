#ifndef consCell_h
#define consCell_h

#include "common.h"
#include "helperLibraries/stringStuff.h"
#include <stdbool.h>

typedef struct __ConsCell {
    Value *car;
    Value *cdr;
} ConsCell;

void freeConsList(ConsCell *node);

void printConsCell(ConsCell *node);

void printValue(Value *value);

void printValueln(Value *value);

//Reverses the tokenization and prints it to the console.
void printTokenizedInput(ConsCell *node, bool unescape, bool quoteString);

//Reverses the tokenization and prints it to the console with a newline at the end.
void printTokenizedInputln(ConsCell *node, bool unescape, bool quoteString);

//Prints the value w/o the type tag.
void printConsValue(Value *value, bool unescape, bool quoteString);

void printConsValueln(Value *value, bool unescape, bool quoteString);

int lengthOfConsCell(ConsCell *node);

ConsCell *makeConsCell();

#endif