#include "stringStuff.h"
#include <stdio.h>

int lengthOfString(char *string) {
	int i = 0;
	for (; string[i]; i++) {}
	return i;
}

char *numberToString(long long number) {
	int numLength = 0;
	long long num = (long long) number;
	do {
		numLength++;
		num /= 10;
	}while(num >= 10);

	char *output = malloc(sizeof(char *) * (numLength + 2));
	sprintf(output, "%lli", number);
	return output;
}

char *doubleToString(double number) {
	char numberString[305];

	sprintf(numberString, "%.*g", 300, number);
	int length = lengthOfString(numberString);
	char *output = (char *) malloc(sizeof(char *) * (length + 1));
	int i = 0;
	for(; i < length; i++)
		output[i] = numberString[i];
	output[i] = '\0';
	return output;
}

bool equalSubstring(char *string, int startIndex, char *substring) {
	for (int i = 0; string[startIndex] && substring[i]; i++, startIndex++)
		if (string[startIndex] != substring[i])
			return false;
	return true;
}

char *substring(char *string, int startIndex, int endIndex) {
	if (endIndex > lengthOfString(string))
		return NULL;
	char *substring = (char *) malloc(sizeof(char *) * (endIndex - startIndex + 1));
	int i = 0;
	for (; i + startIndex < endIndex; i++)
		substring[i] = string[startIndex + i];
	substring[i] = '\0';
	return substring;
}

int getEndParen(char *string, int startIndex) {
	return getEndIndex(string, startIndex, '(', ')');
}

int getEndIndex(char *string, int startIndex, char openSymbol, char closeSymbol) {
	if (startIndex + 1 >= lengthOfString(string))
		return -1;

	int endIndex = startIndex + 1, openCount = 1;
	if (string[startIndex] != openSymbol)
		return -1;

	for (; string[endIndex] && openCount > 0; endIndex++) {
		if (string[endIndex] == openSymbol)
			openCount++;
		else if (string[endIndex] == closeSymbol)
			openCount--;
		if (openCount == 0)
			return endIndex;
	}
	if (openCount != 0)
		return -1;
	return endIndex;
}

int getEndIndexGeneral(char *string, int startIndex, char *openSymbol, char *closeSymbol) {
	int openLength = lengthOfString(openSymbol);
	int closeLength = lengthOfString(closeSymbol);
	int endIndex = startIndex + openLength, openCount = 1;

	if (startIndex + openLength + closeLength > lengthOfString(string))
		return -1;

	for (; string[endIndex + closeLength] && openCount > 0; endIndex++) {
		if (string[endIndex + openLength] && equalSubstring(string, endIndex, openSymbol))
			openCount++;
		else if (string[endIndex + closeLength] && equalSubstring(string, endIndex, closeSymbol))
			openCount--;
		if(openCount == 0)
			return endIndex;
	}

	if (openCount != 0)
		return -1;
	return endIndex;
}

char *concatenate(char *string1, char *string2) {
	int length1 = lengthOfString(string1), length2 = lengthOfString(string2);

	char *output = (char *) malloc(sizeof(char *) * (length1 + length2 + 1));
	int i = 0;

	for(; i < length1; i++)
		output[i] = string1[i];
	for(; i < length1 + length2; i++)
		output[i] = string2[i-length1];

	output[i] = '\0';
	return output;
}

char *duplicateString(char *string) {
	char *output = (char *) malloc(sizeof(char *) * lengthOfString(string));
	int i = 0;
	for (; string[i]; i++)
		output[i] = string[i];
	output[i] = '\0';
	return output;
}