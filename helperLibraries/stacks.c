#include "stacks.h"

ConsCellStack *makeConsCellStack() {
	ConsCellStack *output = (ConsCellStack *) malloc(sizeof(ConsCellStack));
	output->size = 0;
	output->maxSize = 10;
	output->stack = (ConsCell **) malloc(sizeof(ConsCell *) * output->maxSize);
	return output;
}

void pushToConsCellStack(ConsCell *node, ConsCellStack *stack) {
	if (stack->size + 1 >= stack->maxSize) {
		stack->stack = (ConsCell **) realloc(stack->stack, stack->maxSize * 2 * sizeof(ConsCell *));
		stack->maxSize = stack->maxSize * 2;
	}
	stack->stack[stack->size] = node;
	stack->size = stack->size + 1;
}

//Remember to malloc space for error in the outer function!
ConsCell *popFromConsCellStack(ConsCellStack *stack, int *error) {
	if (stack->size <= 0) {
		*error = STACK_EMPTY_EXCEPTION;
		return NULL;
	}
	stack->size = stack->size - 1;
	ConsCell *node = stack->stack[stack->size];
	*error = STACK_NORMAL;
	return node;
}

void freeConsCellStack(ConsCellStack *stack) {
	if (stack == NULL)
		return;
	free(stack->stack);
	free(stack);
}

DepthStack *makeDepthStack() {
	DepthStack *output = (DepthStack *) malloc(sizeof(DepthStack));
	output->size = 0;
	output->maxSize = 10;
	output->stack = (int *) malloc(sizeof(int) * output->maxSize);
	return output;
}

void pushToDepthStack(int depth, DepthStack *stack) {
	if (stack->size + 1 >= stack->maxSize) {
		stack->stack = (int *) realloc(stack->stack, stack->maxSize * 2 * sizeof(int));
		stack->maxSize = stack->maxSize * 2;
	}
	stack->stack[stack->size] = depth;
	stack->size = stack->size + 1;
}

int popFromDepthStack(DepthStack *stack) {
	stack->size = stack->size - 1;
	return stack->stack[stack->size];
}

int peekFromDepthStack(DepthStack *stack) {
	return stack->stack[stack->size - 1];
}

void freeDepthStack(DepthStack *stack) {
	if (stack == NULL)
		return;
	free(stack->stack);
	free(stack);	
}