/*
 * A system of dealing with null-terminated Strings
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * Returns the length of the string.
 */
int lengthOfString(char *string);

//ONLY PRINTS NON-DECIMAL NUMBERS
char *numberToString(long long number);

//NOTE: THIS HAS A 300 Digit maximum
char *doubleToString(double number);

/*
 * Compares the substring of string from startIndex to the end of substring.
 */
bool equalSubstring(char *string, int startIndex, char *substring);

/*
 * Returns the substring of string from startIndex to endIndex - 1.
 * i.e. substring(string, 0, lengthOfString(string)) returns the whole string.
 * If endIndex is greater than the length of the string (array out of bounds), it returns null.
 */
char *substring(char *string, int startIndex, int endIndex);

/*
 * Forwards to getEndIndex with '(' and ')' as the opening and closing symbols respectively.
 */
int getEndParen(char *string, int startIndex);

/*
 * Finds and returns the index of the closeSymbol that corresponds to the given opening symbol.
 * If the character at startIndex is not an instance of the opening symbol, or no corresponding closeSymbol was found, it returns -1.
 */
int getEndIndex(char *string, int startIndex, char openSymbol, char closeSymbol);

/*
 * Less efficient than getEndIndex, but allows for multi-character open and close symbols.
 * startIndex is the index of the first character in the openSymbol.
 */
int getEndIndexGeneral(char *string, int startIndex, char *openSymbol, char *closeSymbol);

/*
 * Combines two strings and returns the result->  NOTE: THIS DOES NOT FREE THE SUBSTRINGS
 */
char *concatenate(char *string1, char *string2);

char *duplicateString(char *string);