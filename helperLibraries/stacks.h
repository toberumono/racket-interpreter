#ifndef stacks_h
#define stacks_h

#include "../common.h"
#include "../consCell.h"
#include <stdlib.h>
#include <stdio.h>

static int STACK_NORMAL = 0;
static int STACK_EMPTY_EXCEPTION = -1;

typedef struct __ConsCellStack {
	int size;
	int maxSize;

	ConsCell** stack;
} ConsCellStack;

typedef struct __DepthStack {
	int size;
	int maxSize;

	int *stack;
} DepthStack;

//Mallocs a new stack with room for 10 items
ConsCellStack *makeConsCellStack();

void pushToConsCellStack(ConsCell *node, ConsCellStack *stack);

ConsCell *popFromConsCellStack(ConsCellStack *stack, int *error);

void freeConsCellStack(ConsCellStack *stack);

//Mallocs a new stack with room for 10 items
DepthStack *makeDepthStack();

void pushToDepthStack(int depth, DepthStack *stack);

int popFromDepthStack(DepthStack *stack);

int peekFromDepthStack(DepthStack *stack);

void freeDepthStack(DepthStack *stack);

#endif