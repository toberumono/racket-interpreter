/* Interface for escaping and un-escaping strings */

/* This method replaces special characters in the string with their
   escaped two-character versions.

   This method allocates and returns a string, which must be freed by
   the caller.
 */
char *escape(char *unescaped);

/* This method replaces escaped characters with their one-character
   equivalents.

   This method allocates and returns a string, which must be freed by
   the caller.
*/
char *unescape(char *escaped);
