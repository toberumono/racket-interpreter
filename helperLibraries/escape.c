/* Implementation of the escape and unescape functions */

//By Julia Kroll and Josh Lipstone

#include <stdlib.h>
#include <stdio.h>

#include "escape.h"

/* This method replaces special characters in the string with their
   escaped two-character versions. */
char *escape(char *unescaped) {
	int length = 0;
	int i = 0;
	for (; unescaped[i]; i++) { //Count length of target string, adding space for two chars for each escape character.
		char current = unescaped[i];
		if(current == '\n' || current == '\t' || current == '\\' || current == '\'' || current == '\"')
			length++;
	}
	length += i;
	char *newString = (char *) malloc ((length + 1) * sizeof(char)); // add one to make room for \0
	int charIndex = 0;
	for (int i = 0; unescaped[i]; i++) {
		char current = unescaped[i];
		switch (current) {
			case '\n': //Add the \ to the string, then add the other half of the escape character in the next slot.
				newString[charIndex] = '\\';
				newString[charIndex + 1] = 'n';
				charIndex += 2; //Increment the insert index appropriately.
				break;
			case '\t':
				newString[charIndex] = '\\';
				newString[charIndex + 1] = 't';
				charIndex += 2;
				break;
			case '\\':
				newString[charIndex] = '\\';
				newString[charIndex + 1] = '\\';
				charIndex += 2;
				break;
			case '\'':
				newString[charIndex] = '\\';
				newString[charIndex + 1] = '\'';
				charIndex += 2;
				break;
			case '\"':
				newString[charIndex] = '\\';
				newString[charIndex + 1] = '\"';
				charIndex += 2;
				break;
			default: //If not an escape character, simply add character and increment charIndex.
				newString[charIndex] = current;
				charIndex += 1;
				break;
		}
	}
	newString[charIndex] = '\0'; // Add null terminator to end of string.
	return newString;
}

/* This method replaces escaped characters with their one-character
   equivalents. */
char *unescape(char *escaped) {
	int length = 0;
	for (int i = 0; escaped[i]; i++) { //Count length of target string, adding one for each normal char and none for '\\'
									   // as the string will be shrinking so escape chars take up one char, not two.
		char current = escaped[i];
		char next = escaped[i + 1];
		switch (current) {
			case '\\':
				break;
			default:
				length += 1;
				break;
		}
	}
	char *newString = (char *) malloc ((length + 1) * sizeof(char *)); // add one to make room for \0
	int charIndex = 0;
	for (int i = 0; escaped[i]; i++, charIndex++) {
		char current = escaped[i];
		char next = escaped[i + 1];
		switch (current) { 
			case '\\': // If current is '\\' then check the following char, 
						   // and add the appropriate escape char to the string.
				switch (next) {
					case 'n':
						newString[charIndex] = '\n';
						break;
					case 't':
						newString[charIndex] = '\t';
						break;
					case '\\':
						newString[charIndex] = '\\';
						break;
					case '\'':
						newString[charIndex] = '\'';
						break;
					case '\"':
						newString[charIndex] = '\"';
						break;
					default:
						newString[charIndex] = '\\';
						i--;
						break;
				}
				i++;
				break;
			default:
				newString[charIndex] = current;
				break;
		}
	}
	newString[charIndex] = '\0'; // Add null terminator to end of string.
	return newString;
}
