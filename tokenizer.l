/* Tokenizer for the Scheme interpreter. */

%{

/* DEFINITIONS - This section contains C code to be included before
   yylex(). You can use it to include libraries, define helper
   functions, etc. */

#include <stdio.h>
#include <string.h>
#include "helperLibraries/escape.h"
#include "helperLibraries/stacks.h"

#include "common.h"
#include "consCell.h"
#include "function.h"

ConsCell *tokens = NULL;
ConsCell *currentCell = NULL;
ConsCellStack *pointerStack = NULL;
DepthStack *depthStack = NULL;
Value *currentToken = NULL;
bool freezeAscent = false, mallocCurrent = true, step = true;
int depth = 0, quoteDepth = -1;


void addCurrentToken() {
    /* Add currentToken to the list. */
    if (tokens == NULL) { // if first token, malloc space for it
        tokens = makeConsCell();
        currentCell = tokens;
    }
    else if (step) { // if this is not the first token, malloc space for the new token and move the stack pointer 
           // to the new token
        currentCell->cdr->type = consType;
        currentCell->cdr->consValue = makeConsCell();
        currentCell = currentCell->cdr->consValue;
    }
    else
        step = true;
    currentCell->car = currentToken;
    mallocCurrent = true;
    if(quoteDepth != -1 && depth >= quoteDepth)
        currentCell->car->quoted = true;
    if(!freezeAscent && depthStack != NULL) {
        while (depthStack->size > 0 && depth == peekFromDepthStack(depthStack)) {
            ascend();
            popFromDepthStack(depthStack);
        }
    }
}

void clearState() {
    step = true;
    depth = 0;
    if(!mallocCurrent)
        freeValue(currentToken);
    freezeAscent = false;
    if (depthStack != NULL)
        freeDepthStack(depthStack);
    depthStack = NULL;
    quoteDepth = -1;
}

void resetTokenizer() {
    clearState();
    if (tokens != NULL)
        freeConsList(tokens);
    if (pointerStack != NULL)
        freeConsCellStack(pointerStack);
    tokens = NULL;
    currentCell = NULL;
    pointerStack = NULL;
}

Value *makeValue() { // used to initialize types to uninitType
    Value *output = (Value *) malloc(sizeof(Value));
    output->type = uninitType;
    output->quoted = false;
    output->accessible = true;
    output->listed = false;
    output->next = NULL;
    return output;
}

Value *duplicateValue(Value *value) {
    Value *result = makeValue();
    result->quoted = value->quoted;
    result->type = value->type;
    switch(value->type) {
        case booleanType:
            result->booleanValue = value->booleanValue;
            return result;
        case integerType:
            result->integerValue = value->integerValue;
            return result;
        case realType:
            result->realValue = value->realValue;
            return result;
        case stringType:
            result->stringValue = duplicateString(value->stringValue);
            return result;
        case consType:
            result->consValue = (ConsCell *) malloc(sizeof(ConsCell));
            result->consValue->car = duplicateValue(value->consValue->car);
            result->consValue->cdr = duplicateValue(value->consValue->cdr);
            return result;
        case identifierType:
            result->identifierValue = duplicateString(value->identifierValue);
            return result;
        case functionType:
            result->functionValue = duplicateFunction(value->functionValue);
            return result;
        default:
            return result;
    }
}

int descend() {
    depth++;
    if (pointerStack == NULL) //If this is the first open paren, initialize the stack
        pointerStack = makeConsCellStack();
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }
    currentToken->type = consType;
    currentToken->consValue = makeConsCell();
    addCurrentToken();
    pushToConsCellStack(currentCell, pointerStack);
    currentCell = currentCell->car->consValue;
    step = false; //The first token matched after descending a level should not
                //cause the currentValue function to step forward.
    return 1;
}

int ascend() {
    depth--;
    if (pointerStack == NULL) { //If this is the first close paren, there is a syntax problem
        printf("syntax error: too many close parentheses\n");
        resetTokenizer();
        exit(EXIT_FAILURE);
        return 0;
    }
    if (!step) //If this is an empty set of parens
        currentCell->car = makeValue();
    int *error = (int *) malloc(sizeof(int));
    currentCell = popFromConsCellStack(pointerStack, error);
    int e = *error;
    free(error);
    if (e == STACK_EMPTY_EXCEPTION) {
        printf("syntax error: too many close parentheses\n");
        resetTokenizer();
        exit(EXIT_FAILURE);
        return 0;
    }
    step = true;
    if(depthStack != NULL) {
        while (depthStack->size > 0 && depth == peekFromDepthStack(depthStack)) {
            ascend();
            popFromDepthStack(depthStack);
        }
    }
    if(depth <= quoteDepth)
        quoteDepth = -1;
    return 1;
}

void descendAndReplace(char *replacement) {
    freezeAscent = true;
    descend();
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }
    currentToken->type = identifierType;
    currentToken->identifierValue = replacement;
    addCurrentToken();
    if(depthStack == NULL)
        depthStack = makeDepthStack();
    pushToDepthStack(depth, depthStack);
    freezeAscent = false;
}

void freeValue(Value *value) {
    if (value->accessible && value->listed)
        return;
    switch (value->type) { // frees the malloc'ed cdr if its type is string, cons, identifier, or function Type
        case stringType:
            free(value->stringValue);
            break;
        case consType:
            freeConsList(value->consValue);
            break;
        case identifierType:
            free(value->identifierValue);
            break;
        case functionType:
            freeFunction(value->functionValue);
            break;
        default:
            break;
    }
    free(value);
}

void switchStream(FILE *stream) {
    yyrestart(stream);
}

void printREPLPrompt() {
    printf(depth ? ". " : "> ");
    for (int i = 0; i < depth; i++)
        printf("   ");
}

%}

/* OPTIONS - Comment these lines out in order to get the yywrap, input
   or unput functions. */
%option noyywrap
%option nounput
%option noinput

%%

 /* RULES */

\( {
    /* open paren */
    freezeAscent = true;
    descend();
    freezeAscent = false;
}

\) {
    /* close paren */
    ascend();
}

\;[^\n]* {
    /* single-line comment */
}

#\|[^(\|#)]*\|# {
    /* multi-line comment */
}

\"([^\"]|\\\")*\" { // String = any characters except unescaped double quotes 
                    // contained between double quotes
    /* string */
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }
    currentToken->type = stringType;
    char *match = (char *) malloc (((yyleng - 2) ? yyleng - 2 : 1) * sizeof(char *));
    int i = 1;
    for (; yytext[i + 1]; i++) // strips quotes from stored string
        match[i - 1] = yytext[i];
    match[i - 1] = '\0';
    char *temp = unescape(match);
    free(match);
    currentToken->stringValue = temp;
    addCurrentToken();
}

\#(t|f) { // boolean = # followed by t or f
    /* boolean */
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }
    currentToken->type = booleanType;
    if (yytext[1] == 't')
        currentToken->booleanValue = true;
    else
        currentToken->booleanValue = false;
    addCurrentToken();
}

(\+|\-)?[0-9]+ { // integer = one or more digits preceded by an optional + or - sign
    /* integer */
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }
    currentToken->type = integerType;
    currentToken->integerValue = atoi(yytext);
    addCurrentToken();
}

(\+|\-)?(\.[0-9]+|[0-9]+\.[0-9]*) { // real = optional + or - sign, followed by either
                                    // a dot followed by at least one digit, or
                                    // at least one digit followed by a dot and zero or more digits
    /* real */
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }
    currentToken->type = realType;
    currentToken->realValue = atof(yytext);
    addCurrentToken();
}

' {
    if(quoteDepth == -1 || depth < quoteDepth)
        quoteDepth = depth;
    /* quoting */
    char *temp = "quote";
    char *quote = (char *) malloc(6 * sizeof(char *));
    int i = 0;
    for(; temp[i]; i++)
        quote[i] = temp[i];
    quote[i] = '\0';
    descendAndReplace(quote);

}

! {
    /* not */
    if (quoteDepth == -1 || depth <= quoteDepth) {
        char *temp = "not";
        char *not = (char *) malloc(4 * sizeof(char *));
        int i = 0;
        for(; temp[i]; i++)
            not[i] = temp[i];
        not[i] = '\0';
        descendAndReplace(not);
    }
    else {
        if (mallocCurrent) {
            currentToken = makeValue();
            mallocCurrent = false;
        }
        currentToken->type = identifierType;
        char *not = (char *) malloc(2 * sizeof(char *));
        not[0] = '!';
        not[1] = '\0';
        currentToken->identifierValue = not;
        addCurrentToken();
    }
}

\n {
    /* newline */
    return 1;
}

[\t ] {
    /* tab, space */
    //Don't actually need to do anything here
}

([a-zA-Z!$%&*/:<=>?~_\^][a-zA-Z_0-9\.+\-!$%&*/:<=>?~_\^]*|\+|\-|\.) { // identifier = either lone + or - sign,
                                            // or initial (letter or ! $ % & * / : < = > ? ~ _ ^) 
                                            // followed by zero or more subsequence (same as initial, along with digits, +, -)
    /* identifier */
    if (mallocCurrent) {
        currentToken = makeValue();
        mallocCurrent = false;
    }    currentToken->type = identifierType;
    char *match = (char *) malloc (yyleng * sizeof(char *)); 
    int i = 0;
    for (; yytext[i]; i++)
        match[i] = yytext[i];
    match[i] = '\0';
    bool doCadr = false;
    if(!strcmp(yytext, "quote")) {
        if(quoteDepth == -1 || depth < quoteDepth)
            quoteDepth = depth;
    }
    else if (yytext[0] == 'c' && lengthOfString(yytext) >= 3 && (quoteDepth == -1 || depth <= quoteDepth)) {
        doCadr = true;
        int length = lengthOfString(yytext);
        if (getEndIndex(yytext, 0, 'c', 'r') != length - 1 || length < 3)
            doCadr = false;
        char *ad = substring(yytext, 1, length - 1);
        for (int i = 0; ad[i]; i++) //Check the string first
            if (ad[i] != 'a' && ad[i] != 'd')
                doCadr = false;
        if (doCadr) {
            char *cadr = (char *) malloc(4 * sizeof(char *));
            cadr[0] = 'c';
            cadr[1] = ad[0];
            cadr[2] = 'r';
            cadr[3] = '\0';
            currentToken->type = identifierType;
            currentToken->identifierValue = cadr;
            addCurrentToken();
            for (int i = 1; ad[i]; i++) {
                cadr = (char *) malloc(4 * sizeof(char *));
                cadr[0] = 'c';
                cadr[1] = ad[i];
                cadr[2] = 'r';
                cadr[3] = '\0';
                descendAndReplace(cadr);
            }
            free(match);
        }
        free(ad);
    }
    if (!doCadr) {
        currentToken->identifierValue = match;
        addCurrentToken();
    }
}

<<EOF>> {
    if (pointerStack != NULL && pointerStack->size != 0) {
        printf("syntax error: not enough close parentheses\n");
        resetTokenizer();
        exit(EXIT_FAILURE);
    }
    return 0;
}

. { // if the tokenizer cannot match the code to a token defined above, 
    // it is a syntax error, exits program
    /* other */
    printf("%s\n", yytext);
    printf("syntax error\n");
    resetTokenizer();
    exit(EXIT_FAILURE);
}
