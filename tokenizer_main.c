#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

#include "common.h"
#include "consCell.h"
#include "interpreter.h"
#include "frame.h"
#include "helperLibraries/stacks.h"

/* The extern qualifier here means that tokens is defined in
   another file. */
extern ConsCell *tokens;
ConsCell *head = NULL;
extern FILE *yyin;
extern int depth;

int main(int argc, char **argv) {
	do {
		if (tokens != NULL && !depth) {
			if (head == NULL)
				head = tokens;
			else if (head->cdr->type == consType)
				head = head->cdr->consValue;
			else {
				if (yyin == NULL || (yyin != NULL && isatty(fileno(yyin))))
					printREPLPrompt();
				continue;
			}
			clearState();
			eval(head);
			while (head->cdr->type == consType)
				head = head->cdr->consValue;
		}
		if (yyin == NULL || (yyin != NULL && isatty(fileno(yyin))))
			printREPLPrompt();
	} while(yylex());
	
	printf("\n"); //For neatness
	if (head == NULL)
		return 0;
	resetTokenizer();
}