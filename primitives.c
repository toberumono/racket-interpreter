#include <strings.h>
#include <stdbool.h>
#include <math.h>

#include "common.h"
#include "consCell.h"
#include "interpreter.h"
#include "function.h"
#include "primitives.h"
#include "helperLibraries/stringStuff.h"

char *names[] = {"car", "cdr", "not", "cons", "null?", "+", "*", "-", "/", "^", "=", ">=", "<=", "<", ">", "%",
"list", "equal?", "modulo", "string-length", "newline", "substring", "string-append", "empty?", "number?", "append",
"list?", ""};

Value *carPrimitive(Value *expr) {
	if (expr->type != consType)
		evalError("car expects a list or pair");
	expr = unquote(expr->consValue->car);
	if (expr->type != consType || expr->consValue->car == NULL || expr->consValue->car->type == uninitType)
		evalError("car expects a list or pair");
	return expr->consValue->car;
}

Value *cdrPrimitive(Value *expr) {
	if (expr->type != consType)
		evalError("cdr expects a list or pair");
	if (expr->consValue->car->type == uninitType)
		evalError("cdr expects a non-null list or pair");
	expr = unquote(expr->consValue->car);
	if (expr->type != consType)
		evalError("cdr expects a list or pair");
	if (expr->consValue->car->type == uninitType)
		evalError("cdr expects a non-null list or pair");
	expr->consValue->cdr->quoted = true;
	if (expr->consValue->cdr->type == uninitType) {
		Value *result = makeValue();
		result->quoted = true;
		result->type = consType;
		result->consValue = makeConsCell();
		result->consValue->car = makeValue();
		pushToValueStack(result);
		return result;
	}
	return expr->consValue->cdr;
}

Value *notPrimitive(Value *expr) {
	basicChecks(expr);
	Value *output = makeValue();
	if (expr->consValue->car->type == booleanType)
		output->booleanValue = !(expr->consValue->car->booleanValue);
	else
		output->booleanValue = false;
	output->quoted = expr->consValue->car->quoted;
	output->type = booleanType;
	pushToValueStack(output);
	return output;
}

Value *consPrimitive(Value *expr) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 2)
		evalError("cons takes 2 arguments");
	Value *result = makeValue();
	result->type = consType;
	result->consValue = (ConsCell *) malloc(sizeof(ConsCell));
	result->consValue->car = duplicateValue(expr->consValue->car);
	expr = unquote(expr->consValue->cdr->consValue->car);
	result->consValue->cdr = duplicateValue(expr);
	result->quoted = true;
	pushToValueStack(result);
	return result;
}

Value *isNullPrimitive(Value *expr) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 1)
		evalError("null? takes 1 argument");
	Value *result = makeValue();
	result->type = booleanType;
	if (expr->consValue->car->type == consType) {
		if (isQuoted(expr->consValue->car)) {
			if (expr->consValue->car->consValue->cdr->consValue->car->type != consType)
				result->booleanValue = false;
			else
				result->booleanValue = expr->consValue->car->consValue->cdr->consValue->car->consValue->car->type == uninitType;
		}
		else
			result->booleanValue = expr->consValue->car->consValue->car->type == uninitType;
	}
	else
		result->booleanValue = false;
	pushToValueStack(result);
	return result;
}

Value *additionPrimitive(Value *expr) {
	double result = 0;
	bool returnReal = false;
	while (expr->type == consType) {
		if (expr->consValue->car->type == realType)
			returnReal = true;
		else if (expr->consValue->car->type != integerType)
			evalError("All arguments for the + operator must be numbers");
		if (expr->consValue->car->type == realType)
			result += expr->consValue->car->realValue;
		else
			result += expr->consValue->car->integerValue;

		expr = expr->consValue->cdr;
	}
	Value *output = makeValue();
	pushToValueStack(output);
	if (returnReal) {
		output->type = realType;
		output->realValue = result;
		return output;
	}
	output->type = integerType;
	output->integerValue = (int)floor(result);
	return output;
}

Value *multiplicationPrimitive(Value *expr) {
	double result = 1;
	bool returnReal = false;
	while (expr->type == consType) {
		if (expr->consValue->car->type == realType)
			returnReal = true;
		else if (expr->consValue->car->type != integerType)
			evalError("All arguments for the * operator must be numbers");
		if (expr->consValue->car->type == realType)
			result *= expr->consValue->car->realValue;
		else
			result *= expr->consValue->car->integerValue;

		expr = expr->consValue->cdr;
	}
	Value *output = makeValue();
	pushToValueStack(output);
	if (returnReal) {
		output->type = realType;
		output->realValue = result;
		return output;
	}
	output->type = integerType;
	output->integerValue = (int)floor(result);
	return output;
}

Value *subtractionPrimitive(Value *expr) {
	double result = 0;
	bool returnReal = false;
	if (expr->type != consType || (expr->type == consType && lengthOfConsCell(expr->consValue) == 0))
		evalError("The - operator takes at least 1 argument");
	if (expr->type == consType && lengthOfConsCell(expr->consValue) > 1) {
		if (expr->consValue->car->type != integerType && expr->consValue->car->type != realType)
			evalError("All arguments for the - operator must be numbers");
		if (expr->consValue->car->type == realType) {
			result = expr->consValue->car->realValue;
			returnReal = true;
		}
		else
			result = expr->consValue->car->integerValue;
		expr = expr->consValue->cdr;
	}

	while (expr->type == consType) {
		if (expr->consValue->car->type != integerType && expr->consValue->car->type != realType)
			evalError("All arguments for the - operator must be numbers");
		if (expr->consValue->car->type == realType) {
			result -= expr->consValue->car->realValue;
			returnReal = true;
		}
		else
			result -= expr->consValue->car->integerValue;
		expr = expr->consValue->cdr;
	}
	Value *output = makeValue();
	pushToValueStack(output);
	if (returnReal) {
		output->type = realType;
		output->realValue = result;
		return output;
	}
	output->type = integerType;
	output->integerValue = (int)floor(result);
	return output;
}

Value *divisionPrimitive(Value *expr) {
	double result = 1;
	bool returnReal = false;
	if (expr->type != consType || (expr->type == consType && lengthOfConsCell(expr->consValue) == 0))
		evalError("The / operator takes at least 1 argument");
	if (expr->type == consType && lengthOfConsCell(expr->consValue) > 1) {
		if (expr->consValue->car->type != integerType && expr->consValue->car->type != realType)
			evalError("All arguments for the / operator must be numbers");
		if (expr->consValue->car->type == realType) {
			result = expr->consValue->car->realValue;
			returnReal = true;
		}
		else
			result = expr->consValue->car->integerValue;
		expr = expr->consValue->cdr;
	}

	while (expr->type == consType) {
		if (expr->consValue->car->type != integerType && expr->consValue->car->type != realType)
			evalError("All arguments for the / operator must be numbers");
		if (expr->consValue->car->type == realType) {
			result /= expr->consValue->car->realValue;
			returnReal = true;
		}
		else {
			if (!returnReal && (int)result % expr->consValue->car->integerValue)
				returnReal = true;
			result /= expr->consValue->car->integerValue;
		}

		expr = expr->consValue->cdr;
	}
	Value *output = makeValue();
	pushToValueStack(output);
	if (returnReal) {
		output->type = realType;
		output->realValue = result;
		return output;
	}
	output->type = integerType;
	output->integerValue = (int)floor(result);
	return output;
}

Value *exponentPrimitive(Value *expr) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 2)
		evalError("the ^ function takes 2 numeric arguments");
	if ((expr->consValue->car->type != integerType && expr->consValue->car->type != realType) || (expr->consValue->cdr->consValue->car->type != integerType && expr->consValue->cdr->consValue->car->type != realType))
		evalError("the ^ function takes 2 numeric arguments");
	bool returnReal = expr->consValue->car->type == realType || expr->consValue->cdr->consValue->car->type == realType;
	double result = pow(((expr->consValue->car->type == realType) ? expr->consValue->car->realValue : expr->consValue->car->integerValue),
	                    ((expr->consValue->cdr->consValue->car->type == realType) ? expr->consValue->cdr->consValue->car->realValue : expr->consValue->cdr->consValue->car->integerValue));
	Value *output = makeValue();
	pushToValueStack(output);
	if (returnReal) {
		output->type = realType;
		output->realValue = result;
		return output;
	}
	output->type = integerType;
	output->integerValue = (int)floor(result);
	return output;
}

Value *comparativeLoop(Value *expr, char *sign, bool (*comparison)(double, double)) {
	double base = 0;
	bool first = true, result = true;
	while (expr->type == consType && expr->consValue != NULL) {
		if (expr->consValue->car->type != integerType && expr->consValue->car->type != realType) {
			result = false;
			break;
		}
		if (expr->consValue->car->type == realType) {
			if (first)
				base = expr->consValue->car->realValue;
			else {
				result = comparison(base, expr->consValue->car->realValue);
				if (!result)
					break;
			}
		}
		else {
			if (first)
				base = expr->consValue->car->integerValue;
			else {
				result = comparison(base, expr->consValue->car->integerValue);
				if (!result)
					break;
			}
		}
		first = false;

		if (expr->consValue->cdr->type != consType)
			break;
		expr = expr->consValue->cdr;
	}

	Value *output = makeValue();
	pushToValueStack(output);
	output->type = booleanType;
	output->booleanValue = result;
	return output;
}

bool eq(double num1, double num2) {
	return num1 == num2;
}

Value *equalToPrimitive(Value *expr) {
	return comparativeLoop(expr, "=", &eq);
}

bool gteq(double num1, double num2) {
	return num1 >= num2;
}

Value *greaterThanEqualToPrimitive(Value *expr) {
	return comparativeLoop(expr, ">=", &gteq);
}

bool lteq(double num1, double num2) {
	return num1 <= num2;
}

Value *lessThanEqualToPrimitive(Value *expr) {
	return comparativeLoop(expr, "<=", &lteq);
}

bool lt(double num1, double num2) {
	return num1 < num2;
}

Value *lessThanPrimitive(Value *expr) {
	return comparativeLoop(expr, "<", &lt);
}

bool gt(double num1, double num2) {
	return num1 > num2;
}

Value *greaterThanPrimitive(Value *expr) {
	return comparativeLoop(expr, ">", &gt);
}

Value *moduloPrimitive(Value *expr) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 2)
		evalError("the % function takes 2 numeric arguments");
	if ((expr->consValue->car->type != integerType && expr->consValue->car->type != realType) || (expr->consValue->cdr->consValue->car->type != integerType && expr->consValue->cdr->consValue->car->type != realType))
		evalError("the % function takes 2 numeric arguments");
	bool returnReal = expr->consValue->car->type == realType || expr->consValue->cdr->consValue->car->type == realType;
	double result = ((int) ((expr->consValue->car->type == realType) ? expr->consValue->car->realValue : expr->consValue->car->integerValue)) %
	                ((int) ((expr->consValue->cdr->consValue->car->type == realType) ? expr->consValue->cdr->consValue->car->realValue : expr->consValue->cdr->consValue->car->integerValue));
	Value *output = makeValue();
	pushToValueStack(output);
	if (returnReal) {
		output->type = realType;
		output->realValue = result;
		return output;
	}
	output->type = integerType;
	output->integerValue = (int)floor(result);
	return output;
}

Value *listPrimitive(Value *expr) {
	if (expr->type == uninitType) {
		Value *output = makeValue();
		output->type = consType;
		output->consValue = makeConsCell();
		output->quoted = true;
		output->consValue->car = makeValue();
		pushToValueStack(output);
		return output;
	}
	Value *list = duplicateValue(expr);
	list->quoted = true;
	pushToValueStack(list);
	Value *head = unquote(list);
	while (head->type == consType) {
		head->consValue->car = unquote(head->consValue->car);
		head = head->consValue->cdr;
	}
	return list;
}

Value *equalPrimitive(Value *expr) { //This way it supports n items
	basicChecks(expr);
	Value *other = expr->consValue->cdr;
	bool outputValue = true;
	while (other->type == consType && expr->type == consType) {
		outputValue = compareValues(unquote(expr->consValue->car), unquote(other->consValue->car));
		if (!outputValue)
			break;
		other = other->consValue->cdr;
	}
	Value *output = makeValue();
	output->type = booleanType;
	output->booleanValue = outputValue;
	pushToValueStack(output);
	return output;
}

Value *stringLengthPrimitive(Value *expr) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 1 || expr->consValue->car->type != stringType)
		evalError("string-length takes a single String as its argument");
	Value *output = makeValue();
	output->type = integerType;
	output->integerValue = lengthOfString(expr->consValue->car->stringValue);
	pushToValueStack(output);
	return output;
}

Value *newlinePrimitive(Value *expr) {
	printf("\n");
	Value *output = makeValue();
	output->type = uninitType;
	pushToValueStack(output);
	return output;
}

Value *substringPrimitive(Value *expr) {
	basicChecks(expr);
	if (lengthOfConsCell(expr->consValue) != 3 || expr->consValue->car->type != stringType || expr->consValue->cdr->consValue->car->type != integerType || expr->consValue->cdr->consValue->cdr->consValue->car->type != integerType)
		evalError("substring takes three arguments of the form: string startIndex endIndex");
	if (expr->consValue->cdr->consValue->car->integerValue < 0 || expr->consValue->cdr->consValue->car->integerValue >= expr->consValue->cdr->consValue->cdr->consValue->car->integerValue)
		evalError("the startIndex of a substring must be greater than 0 and less than the endIndex");
	if (expr->consValue->cdr->consValue->cdr->consValue->car->integerValue > lengthOfString(expr->consValue->car->stringValue))
		evalError("the endIndex of a substring must be less than or equal to the length of the string");
	Value *output = makeValue();
	output->type = stringType;
	output->stringValue = substring(expr->consValue->car->stringValue, expr->consValue->cdr->consValue->car->integerValue, expr->consValue->cdr->consValue->cdr->consValue->car->integerValue);
	pushToValueStack(output);
	return output;
}

Value *stringAppendPrimtive(Value *expr) {
	basicChecks(expr);
	char *result = (char *) malloc(sizeof(char *));
	result[0] = '\0';
	while (expr->type == consType) {
		if (expr->consValue->car->type != stringType)
			evalError("All arguments for string-append must be strings");
		char *temp = result;
		temp = concatenate(result, expr->consValue->car->stringValue);
		free(result);
		result = temp;
		expr = expr->consValue->cdr;
	}
	Value *output = makeValue();
	output->type = stringType;
	output->stringValue = result;
	pushToValueStack(output);
	return output;
}

Value *isNumberPrimitive(Value *expr) {
	basicChecks(expr);
	Value *output = makeValue();
	output->type = booleanType;
	Value *temp = unquote(expr);
	output->booleanValue = temp->consValue->car->type == integerType || temp->consValue->car->type == realType;
	pushToValueStack(output);
	return output;
}

Value *appendPrimitive(Value *expr) {
	basicChecks(expr);
	if (expr->consValue->car->type != consType) {
		Value *output = makeValue();
		output->type = consType;
		output->consValue = makeConsCell();
		output->quoted = true;
		output->consValue->car = makeValue();
		pushToValueStack(output);
		return output;
	}

	Value *output = duplicateValue(unquote(expr->consValue->car));
	expr = expr->consValue->cdr;
	Value *end = output;
	while (expr->type == consType) {
		while (end->consValue->cdr->type == consType)
			end = end->consValue->cdr;
		freeValue(end->consValue->cdr);
		end->consValue->cdr = duplicateValue(unquote(expr->consValue->car));
		if (end->consValue->cdr->type != consType)
			break;
		expr = expr->consValue->cdr;
	}
	if (expr->type == consType && expr->consValue->cdr->type != uninitType) {
		freeValue(output);
		evalError("only the last argument for append can be a non-list type");
	}
	pushToValueStack(output);

	return output;
}

Value *isListPrimitive(Value *expr) {
	basicChecks(expr);
	Value *output = makeValue();
	output->type = booleanType;
	Value *temp = unquote(expr->consValue->car);
	output->booleanValue = temp->type == consType && temp->quoted;
	pushToValueStack(output);
	return output;
}

Value *lengthPrimitive(Value *expr) {
	basicChecks(expr);
	Value *temp = unquote(expr->consValue->car);
	if (temp->type != consType || !temp->quoted)
		evalError("length expects a list");
	Value *output = makeValue();
	output->type = integerType;
	output->integerValue = lengthOfConsCell(temp->consValue);
	pushToValueStack(output);
	return output;
}

void bindPrimitives(Frame *root) {
	addVariable(names[0], makePrimitive(&carPrimitive), root);
	addVariable(names[1], makePrimitive(&cdrPrimitive), root);
	addVariable(names[2], makePrimitive(&notPrimitive), root);
	addVariable(names[3], makePrimitive(&consPrimitive), root);
	addVariable(names[4], makePrimitive(&isNullPrimitive), root);
	addVariable(names[5], makePrimitive(&additionPrimitive), root);
	addVariable(names[6], makePrimitive(&multiplicationPrimitive), root);
	addVariable(names[7], makePrimitive(&subtractionPrimitive), root);
	addVariable(names[8], makePrimitive(&divisionPrimitive), root);
	addVariable(names[9], makePrimitive(&exponentPrimitive), root);
	addVariable(names[10], makePrimitive(&equalToPrimitive), root);
	addVariable(names[11], makePrimitive(&greaterThanEqualToPrimitive), root);
	addVariable(names[12], makePrimitive(&lessThanEqualToPrimitive), root);
	addVariable(names[13], makePrimitive(&lessThanPrimitive), root);
	addVariable(names[14], makePrimitive(&greaterThanPrimitive), root);
	addVariable(names[15], makePrimitive(&moduloPrimitive), root);
	addVariable(names[16], makePrimitive(&listPrimitive), root);
	addVariable(names[17], makePrimitive(&equalPrimitive), root);
	addVariable(names[18], makePrimitive(&moduloPrimitive), root);
	addVariable(names[19], makePrimitive(&stringLengthPrimitive), root);
	addVariable(names[20], makePrimitive(&newlinePrimitive), root);
	addVariable(names[21], makePrimitive(&substringPrimitive), root);
	addVariable(names[22], makePrimitive(&stringAppendPrimtive), root);
	addVariable(names[23], makePrimitive(&isNullPrimitive), root);
	addVariable(names[24], makePrimitive(&isNumberPrimitive), root);
	addVariable(names[25], makePrimitive(&appendPrimitive), root);
	addVariable(names[26], makePrimitive(&isListPrimitive), root);
	addVariable(names[27], makePrimitive(&lengthPrimitive), root);
}

void unbindPrimitives(Frame *root) {
	for (int i = 0; names[i][0]; i++) {
		Value *val = getVariable(names[i], root);
		freeValue(val);
	}
}