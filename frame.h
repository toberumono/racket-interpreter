#ifndef frame_h
#define frame_h

#include <stdbool.h>
#include <stdio.h>

#include "common.h"
#include "function.h"

typedef struct __Variable {
	struct __Variable *next;
	Value *value;
	char *name;
} Variable;

typedef struct __Frame {
	struct __Frame *previous;

	struct __Variable *vars;
	
	bool function, accessible;
} Frame;

typedef struct __FrameStack {
	int size, maxSize;

	Frame **stack;
} FrameStack;

Frame *initFrame(Frame *env);

Variable *initVariable(char *name, Value *value);

void addVariable(char *name, Value *value, Frame *env);

bool setVariable(char *name, Value *value, Frame *env);

Value *getVariable(char *name, Frame *env);

bool containsVariable(char *name, Frame *env);

bool isVariable(char *name, Frame *env);

void freeVariable(struct __Variable *var);

void freeFrame(Frame *env);

Frame *getTopFrame(Frame *env);

void markFunctionEnv(Frame *env);

bool compareFrames(Frame *first, Frame *second);

void sweepFrames();

struct __FrameStack *makeFrameStack();

void pushToFrameStack(Frame *env, FrameStack *stack);

Frame *popFromFrameStack(FrameStack *stack);

void unmarkFrameStack(FrameStack *stack);

void markFrameStack(FrameStack *stack);

void freeFrameStack(FrameStack *stack);

#endif